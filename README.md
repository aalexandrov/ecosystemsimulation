# EcosystemSimulation



## Usage

You will need python 3.7 or higher as well as pipenv!
From inside the code folder follow these instructions to run the simulation.
Create a virtual environment with:
```
pipenv install -r requirements.txt
```
If you encounter errors, delete the Pipfile and remove the environment before trying again with:
```
pipenv --rm
```
Then start the environment with:
```
pipenv shell
```

Inside the code folder run the simulator with:
```
python gui.py
```

The simulation should start in a pygame window and a few seconds later the plots will also start showing and updating in another window.
