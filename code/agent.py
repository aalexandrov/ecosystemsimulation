from __future__ import annotations
import math
import random
import time
import numpy as np

from collections import deque
from environment_objects import *
from utils.math_utils import *
from utils.map_queries import *
from utils.constants import *

# Utility class to identify similar agents
class DNA:
    def __init__(self, agent) -> None:
        self.type = type(agent)
        self.code = (agent.strength, agent.speed, agent.sense, agent.intelligence)

    # Returns True if the two DNA's are considered similar
    def similar(self, other: DNA, max_deviation = 2):
        diff = 0
        for i in range(len(self.code)):
            diff += abs(self.code[i] - other.code[i])
        return self.type == other.type and diff/len(self.code) <= max_deviation

class Agent:
    def __init__(self, grid: list, agents: set, strength: int, speed: int, sense: int, intelligence: int, sociability: int):
        # constants
        self.strength = strength
        self.speed = speed
        self.sense = sense
        self.intelligence = intelligence
        self.sociability = sociability
        self.food_bonus = strength
        self.vision = int(round(MAX_VISION*(self.sense/100)))
        self.memory = int(round(MAX_MEMORY*(self.intelligence/100)))
        self.decision_probability = BASE_DECISION_PROBABILITY + self.intelligence/(100/(1 - BASE_DECISION_PROBABILITY))

        # the agent's memories
        self.enemy_dnas = set()
        self.friendly = set()
        self.prev_positions = deque()

        # dynamic fields
        self.energy = 100
        self.hunger = 0
        self.thirst = 0
        self.sexual = 0
        self.movement_cooldown = 0
        self.position = (0,0)

        # shared data
        self.grid = grid
        self.agents = agents

    def __eq__(self,other):
        if isinstance(other, Agent):
            if id(self) == id(other) :
                return True

        return False

    def __hash__(self):
        return id(self)

    def __str__(self):
        string_builder = str(type(self)) + "Strength {}, Speed {}, Sense {}, Intelligence {}, Sociability {} \n Energy {}, Hunger {}, Thirst {}, Sexual {}".format(self.strength, self.speed, self.sense, self.intelligence, self.sociability, self.energy, self.hunger, self.thirst, self.sexual)
        return string_builder

    def __repr__(self):
        return self.__str__()

    def alive(self):
        """
        return bool value based on liveliness and string for cause of death for data-analyzer
        """
        if self.energy <= 0:
            return False, "energy"
        elif self.hunger >= 100:
            return False, "hunger"
        elif self.thirst >= 100:
            return False, "thirst"
        return True, "_"

    def get_surroundings(self):
        """
        Get a grid of the surroundings based on the agent's vision and its
        relative position in it.
        """
        top_left_x = max(0, self.position[1]-self.vision)
        top_right_x = min(len(self.grid)-1, self.position[1]+self.vision)
        top_left_y = max(0, self.position[0]-self.vision)
        bottom_left_y = min(len(self.grid)-1, self.position[0]+self.vision)

        width = top_right_x - top_left_x + 1
        height = bottom_left_y - top_left_y + 1

        visible_tiles = [(i,j) for j in range(top_left_x,top_right_x+1) for i in range(top_left_y,bottom_left_y+1)]

        relative_position = (0,0)
        for i,j in visible_tiles:
            if self.grid[i][j] == self:
                relative_position = (i - top_left_y, j - top_left_x)

        if relative_position == None:
            raise ValueError('Agent was not inside the vision grid, something is wrong...')

        return visible_tiles, relative_position

    def get_necessities(self):
        """
        Get a list of actions sorted by their necessity.
        """
        priorities = list()
        necessities = list()
        if self.thirst>THIRST_THRESHOLD:
            priorities.append(("drink", self.thirst))

        if self.hunger>HUNGER_THRESHOLD:
            priorities.append(("eat", self.hunger))

        if self.energy<ENERGY_THRESHOLD:
            priorities.append(("rest", 100 - self.energy))

        necessities = list(map(lambda x: x[0], sorted(priorities, key = lambda x: x[1], reverse=True)))
        if MATING_THRESHOLD < self.sexual:
            necessities.append("mate")

        return necessities

    # True if the passed agent is a possible target for hunting
    def possible_target(self, other):
        return not DNA(self).similar(DNA(other), self.sociability * 5 / 100)

    def hunt(self):
        """"
        Find the closest agent to be hunted and attack it or move towards it
        """
        closest_agent_position = get_closest_with((lambda cell: (isinstance(cell, Herbivore) or isinstance(cell, Omnivore)) and self.possible_target(cell)), self.grid, self.position, self.vision)
        if closest_agent_position is not None:
            prey = self.grid[closest_agent_position[0]][closest_agent_position[1]]
            hunt_it = True
            if prey not in self.friendly and prey.strength > self.strength:
                #print("hunt_it =", self.intelligence, " < ", 100 * (prey.strength - self.strength) / (prey.strength + self.strength))
                hunt_it = self.intelligence < 100 * (prey.strength - self.strength) / (prey.strength + self.strength)

            if hunt_it:
                direction = get_direction(self.position, closest_agent_position)
                if neighbours(closest_agent_position, self.position):
                    self.attack(prey)
                else:
                    # print("Hunting...")
                    self.move(direction)

                return True

        return False

    def	make_decision(self):
        """
        Make the best decisions with a probability based on intelligence.
        If the agent dies during the iteration, they don't make any actions.
        The agent prioritizes safety above all, so it first checks if there
        are enemies around.
        """
        directions = [(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1)]
        # field_of_vision, relative_position = self.get_surroundings() # not used as of new implementation, only for debugging
        if not self.alive()[0]:
            return False

        if len(self.enemy_dnas) > 0:
            closest_agent_position = get_closest(Agent, self.grid, self.position, self.vision)
            if closest_agent_position is not None and random.random() < self.decision_probability:
                predator = self.grid[closest_agent_position[0]][closest_agent_position[1]]
                predator_dna = DNA(predator)

                for dna in self.enemy_dnas:
                    if predator_dna.similar(dna):
                        # => The closest agent actually is a predator
                        fleeing_dir = get_direction(closest_agent_position, self.position)
                        self.move(fleeing_dir)
                        return True

        # Check if there are any necessities rn
        necessities = self.get_necessities()
        for necessity in necessities:
            if necessity == "drink":
                close_water_position = get_closest(WaterBody, self.grid, self.position, self.vision)
                if close_water_position is not None and random.random() < self.decision_probability:
                    direction = get_direction(self.position, close_water_position)
                    if neighbours(close_water_position, self.position):
                        water = self.grid[self.position[0] + direction[0]][self.position[1] + direction[1]]
                        self.drink(water)
                    else:
                        self.move(direction)

                    return True

            elif necessity == "eat":
                if isinstance(self, Herbivore) or isinstance(self, Omnivore):
                    close_plant_position = get_closest(Plant, self.grid, self.position, self.vision)
                    if close_plant_position is not None and random.random() < self.decision_probability:
                        direction = get_direction(self.position, close_plant_position)
                        if neighbours(close_plant_position, self.position):
                            plant = self.grid[self.position[0] + direction[0]][self.position[1] + direction[1]]
                            self.eat(plant)
                        else:
                            self.move(direction)

                        return True

                if isinstance(self, Omnivore) or isinstance(self, Predator):
                    closest_meat_position = get_closest(Meat, self.grid, self.position, self.vision)
                    # If the closest meat is insight always prefeer it over a fight
                    # TODO: Maybe make that ^ dependent on the intelligence
                    if closest_meat_position is not None and random.random() < self.decision_probability:
                        # print("Moving towards meat - distance:", distance(self.position, closest_meat_position))
                        direction = get_direction(self.position, closest_meat_position)
                        if neighbours(closest_meat_position, self.position):
                            meat = self.grid[self.position[0] + direction[0]][self.position[1] + direction[1]]
                            self.eat(meat)
                        else:
                            self.move(direction)

                        return True
                    elif isinstance(self, Predator):
                        if self.hunt():
                            return True


            elif necessity == "rest":
                self.rest()
                return True

            elif necessity == "mate":
                closest_mating_position = get_closest(type(self), self.grid, self.position, self.vision, get_mate=MATING_THRESHOLD)
                if closest_mating_position is not None and random.random() < self.decision_probability:
                    direction = get_direction(self.position, closest_mating_position)
                    if neighbours(closest_mating_position, self.position):
                        self.mate(closest_mating_position)
                    else:
                        self.move(direction)
                    return True
            else:
                closest_agent_position = get_closest_with((lambda cell: isinstance(cell, Agent)), self.grid, self.position, self.vision)
                if closest_agent_position is not None:
                    if neighbours(closest_agent_position, self.position):
                        if self.socialize(closest_agent_position):
                            return True

        self.random_move()

        return True

    def socialize(self, other_position):
        """
        Make the agent a friendly target to another agent with probability
        based on sociability.
        """
        other = self.grid[other_position[0]][other_position[1]]
        if not DNA(other) in self.enemy_dnas and random.random() <= self.sociability/200:
            other.friendly.add(self)
            return True

        return False

    def	attack(self, other: Agent):
        """
        Implements the fighting mechanic with advantage on the attacker.

        """
        # other should remember that this type of Agent attacked it
        if random.randint(0, 100) <= other.intelligence:
            other.enemy_dnas.add(DNA(self))

        attacker_modifier = max(0, self.intelligence - other.intelligence) + max(0, self.speed - other.speed)
        if other.strength*2 < self.strength + attacker_modifier:
            other.energy = 0
            self.energy = max(0, self.energy - BASE_DAMAGE)

        elif other.strength > 2*self.strength:
            self.energy = 0
            other.energy = max(0, other.energy - BASE_DAMAGE)

        else:
            other.energy = max(0, self.energy - BASE_DAMAGE - max(0,(self.strength-other.strength)/10))

    def	eat(self, food):
        # Only consume food if it's still available
        if food.food_bonus > 0:
            old_hunger = self.hunger
            self.hunger = max(self.hunger - food.food_bonus, 0)
            # Reduce leftover food_bonus accordingly
            food.food_bonus -= old_hunger - self.hunger

    def	drink(self, water: WaterBody):
        if water.depth > 0:
            old_thirst = self.thirst
            self.thirst = max(self.thirst - water.depth, 0)
            # Reduce leftover depth accordingly
            water.depth -= old_thirst - self.thirst

    def rest(self):
        self.energy += 5

    def get_offspring(self, other):
        """
        Create a child of two agents that takes the average of its combined parent genes and
        adds a small mutation in the genes determined by the MAX_MUTATION_SIZE constant.
        """
        skills1 = np.array([self.strength, self.speed, self.sense, self.intelligence, self.sociability])
        skills2 = np.array([other.strength, other.speed, other.sense, other.intelligence, self.sociability])
        skills_new = np.ceil((skills1 + skills2)/2)
        sum = 0

        # mutate offspring
        mutation = []
        for _ in range(len(skills_new)-1):
            gene_mutation = random.randint(-MAX_MUTATION_SIZE,MAX_MUTATION_SIZE)
            sum += gene_mutation
            mutation.append(gene_mutation)

        mutation.append(-sum) # balance out the genes so that the sum equals 0 e.g. [-5,-2,4,3]
        np.random.shuffle(mutation)
        skills_new -= mutation
        skills_new = remove_skills_overflow(skills_new, AGENT_SKILL_POINTS)
        strength, speed, sense, intelligence, sociability = skills_new[0], skills_new[1], skills_new[2], skills_new[3], skills_new[4]
        new_agent = type(self)(self.grid, self.agents, strength, speed, sense, intelligence, sociability)
        return new_agent


    def	mate(self, other_position):

        # get other agent at position
        other = self.grid[other_position[0]][other_position[1]]

        # add the two vectors and rescale them
        offspring = self.get_offspring(other)

        # find position for new agent
        offspring.position = get_closest(Agent, self.grid, self.position, 100, get_empty=True)
        total_thirst = 200 - (self.thirst + other.thirst)
        self.thirst = 100 - total_thirst/3
        other.thirst = 100 - total_thirst/3
        offspring.thirst = 100 - total_thirst/3
        # add new agent to 'agents' and 'grid'
        self.agents.add(offspring)
        self.grid[offspring.position[0]][offspring.position[1]] = offspring

        # change sexual of both agents
        self.sexual = 0
        other.sexual = 0

    def	move(self, direction: tuple): # direction is one of (0,1), (1,0), (0,-1), (-1,0), (-1,-1), (1,-1), (-1,1), (1,1)
        """
        Current logic for movement is that we move 1 tile at a time with a movement cooldown.
        The greater the speed of the agent, the less cooldown they will experience.
        The agent also has memory of previous tiles they have visited, which is based
        on their intelligence stat. The memory is used for random moves, to avoid going back
        and forth with no progress.
        """
        if self.movement_cooldown == 0:
            self.grid[self.position[0]][self.position[1]] = None
            self.prev_positions.append(self.position)
            if len(self.prev_positions) > self.memory:
                self.prev_positions.popleft()

            y, x = self.position[0] + direction[0], self.position[1] + direction[1]
            # Note: This can throw an IndexError
            if y < 0 or y > len(self.grid) or x < 0 or x > len(self.grid) or self.grid[y][x] is not None: # if the path is blocked, make a random move
                self.random_move()
                return


            self.position = (y,x)
            self.grid[y][x] = self
            self.movement_cooldown = MAX_MOVEMENT_COOLDOWN - round(self.speed/20)
        else:
            self.movement_cooldown -= 1


    def random_move(self):
        """
        Avoids making moves into objects and outside of the grid, also avoids returning to its previous squares,
        based on its memory of visited squares.
        """
        directions = [(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1)]
        available_directions = []
        for direction in directions:
            if self.position[0]+direction[0] in range(0,len(self.grid)) and \
                    self.position[1]+direction[1] in range(0,len(self.grid)) and \
                    self.grid[self.position[0]+direction[0]][self.position[1]+direction[1]] is None:

                available_directions.append(direction)

        no_revists = []
        for direction in available_directions:
            if self.grid[self.position[0]+direction[0]][self.position[1]+direction[1]] not in self.prev_positions:
                no_revists.append(direction)

        if len(available_directions) == 0:
            self.rest()
            return False

        random_dir = random.choice(available_directions)
        if len(no_revists) != 0:
            random_dir = random.choice(no_revists)

        self.move(random_dir)

        return True

class Herbivore(Agent):
	def __init__(self, grid, agents, strength, speed, sense, intelligence, sociability):
		super().__init__(grid, agents, strength, speed, sense, intelligence, sociability)

class Predator(Agent):
	def __init__(self, grid, agents, strength, speed, sense, intelligence, sociability):
		super().__init__(grid, agents, strength, speed, sense, intelligence, sociability)

class Omnivore(Agent):
	def __init__(self, grid, agents, strength, speed, sense, intelligence, sociability):
		super().__init__(grid, agents, strength, speed, sense, intelligence, sociability)
