# EcosystemSimulation



## Usage

You will need python 3.7 or higher!
Create a virtual environment with:
```
pipenv install -r requirements.txt
```
If you encounter errors, delete the Pipfile and remove the environment before trying again with:
```
pipenv --rm
```
Then start the environment with:
```
pipenv shell
```

Run the simulator with:
```
python gui.py
```

The simulation should start in a pygame window and a few seconds later the plots will also start showing and updating in another window.
