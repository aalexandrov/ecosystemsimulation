import numpy as np
import copy
import random
from os import walk

from environment_objects import *
from agent import *
from utils.constants import *
from utils.create_map import *
from utils.data_analyzer import Data
from utils.math_utils import *

assert(WATER_FOR_PLANT_THRESHOLD >= WATER_FOR_PLANT_SUBTRACTION) # else in negative water

class Environment:
	def __init__(self, size, population, terrain, debug_mode=False):

		self.env_size = size
		self.agent_population = population
		self.terrain = terrain
		self.agents = set()
		self.non_agents = set()

		self.ground_grid = [[GroundCell(30, 30) for _ in range(self.env_size)] for _ in range(self.env_size)]
		self.sky_grid = [[SkyCell(10) for _ in range(self.env_size)] for _ in range(self.env_size)]
		self.grid = [[None for _ in range(self.env_size)] for _ in range(self.env_size)]

		self.data_analyzer = Data(self.agents, self.ground_grid, self.grid)
		self.debug_mode = debug_mode

	def _neighbors(self, grid_inst, x, y):
		"""
		Returns a list containing all neighbors of the cell at position x,y in grid_inst
		"""
		neighbors = []
		if x > 0:
			neighbors.append(grid_inst[y][x-1])
		if y > 0:
			neighbors.append(grid_inst[y-1][x])
		if x + 1 < len(grid_inst[0]):
			neighbors.append(grid_inst[y][x + 1])
		if y + 1 < len(grid_inst):
			neighbors.append(grid_inst[y + 1][x])
		return neighbors

	def _spread_value(self, value_name, grid_inst, x, y, spreading_speed = 0.1):
		"""
		Spreads the value with the passed name to neighboring cells of the one at position x,y
		within grid_inst and with spreading_speed
		"""
		cell = grid_inst[y][x]
		neighbors = self._neighbors(grid_inst, x, y)
		# Make value spread into neighboring cells.
		# We keep it simple and just average all the values.
		# Only spread to neighbors with smaller value:
		smaller_neighbors = []
		for neigh in neighbors:
			if getattr(neigh, value_name) < getattr(cell, value_name):
				smaller_neighbors.append(neigh)

		total = getattr(cell, value_name)
		for neigh in smaller_neighbors:
			total += getattr(neigh, value_name)

		avg = total / (1 + len(smaller_neighbors))
		for neigh in smaller_neighbors:
			setattr(neigh, value_name, lin_interp(getattr(neigh, value_name), avg, spreading_speed))
			total -= getattr(neigh, value_name)

		setattr(cell, value_name, total)

	def update(self):
		"""
		Update the environment's non-agent objects.
		"""
		# Ground Grid:
		random_ids = [*range(len(self.ground_grid) * len(self.ground_grid[0]))]
		random.shuffle(random_ids)
		for rand_id in random_ids:
			x = rand_id % len(self.ground_grid[0])
			y = rand_id // len(self.ground_grid)
			cell = self.ground_grid[y][x]

			cell.update()
			# Spread values to neighboring cells
			self._spread_value("water", self.ground_grid, x, y, spreading_speed=WATER_SPREAD_SPEED)
			self._spread_value("nutrition", self.ground_grid, x, y, spreading_speed=NUTRITION_SPREAD_SPEED)

		# Grid:
		random_ids = [*range(len(self.grid) * len(self.grid[0]))]
		random.shuffle(random_ids)
		for rand_id in random_ids:
			x = rand_id % len(self.grid[0])
			y = rand_id // len(self.grid)

			gcell = self.ground_grid[y][x]
			cell = self.grid[y][x]

			# Remove consumed plants/waterbodies or regenerate existing ones
			if isinstance(cell, Plant):
				if cell.food_bonus <= 0:
					# Remove consumed Plant
					self.grid[y][x] = None
				else:
					# Regenerate
					gcell.health, cell.food_bonus = try_transfer(gcell.health, cell.food_bonus, REGENERATION_RATE_HEALTH_FACTOR * gcell.health)

			elif isinstance(self.grid[y][x], WaterBody):
				if cell.depth <= 1:
					# Remove consumed WaterBodies
					self.grid[y][x] = None

			elif isinstance(cell, Meat):
				if cell.food_bonus <= 0:
					# Remove consumed Meat
					self.grid[y][x] = None
				else:
					# Dissolve existing ones
					_, gcell.nutrition = try_transfer(cell.food_bonus, gcell.nutrition, MEAT_DECAY)
					cell.food_bonus -= MEAT_DECAY

			# Spawn new Plants/WaterBodies with some defined chance if conditions are met
			elif cell is None:
				if gcell.water >= WATER_SPAWN_THRESHOLD and self.grid[y][x] is None:
						val = gcell.water
						self.grid[y][x] = WaterBody(val)
						gcell.water -= val

				if random.random() < PLANT_SPAWN_PROBABILITY:
					if gcell.health >= PLANT_SPAWN_THRESHOLD and gcell.water >= WATER_FOR_PLANT_THRESHOLD and self.grid[y][x] is None:
						gcell.water -= WATER_FOR_PLANT_SUBTRACTION
						val = gcell.health * 0.5
						self.grid[y][x] = Plant(val)
						gcell.health -= val

			# Make water condense
			scell = self.sky_grid[y][x]
			gcell.water, scell.water = try_transfer(gcell.water, scell.water, CONDENSATION_RATE * gcell.water)

		# Sky Grid:
		random_ids = [*range(len(self.sky_grid) * len(self.sky_grid[0]))]
		random.shuffle(random_ids)
		for rand_id in random_ids:
			x = rand_id % len(self.sky_grid[0])
			y = rand_id // len(self.sky_grid)
			scell = self.sky_grid[y][x]
			# Rain down with a chance proportional to water value
			if random.randint(0, 100) <= scell.water*2:
				gcell = self.ground_grid[y][x]
				scell.water, gcell.water = try_transfer(scell.water, gcell.water, scell.water)
			else:
				# Spread water to neighboring cells
				self._spread_value("water", self.sky_grid, x, y)

		total_water = self.get_total_water()
		addition_per_cell = (WATER_AMOUNT - total_water)/(self.env_size**2)
		if total_water < WATER_AMOUNT*0.75:
			for i in range(self.env_size):
				for j in range(self.env_size):
					self.sky_grid[i][j].water += addition_per_cell

	def generate(self):
		"""
		Generate the objects in the environment based on the pre-created excel
		terrain.
		"""
		# read the excel file and load it into the ground grid
		grid_excel = map_from_excel(self.terrain)
		if self.debug_mode:
			grid_excel = map_from_excel('./utils/map_debug.xlsx')

		for i in range(len(grid_excel)):
			for j in range(len(grid_excel[0])):
				if grid_excel[i][j] == 'P':
					self.grid[i][j] = Plant(random.randint(20, 30))
				if grid_excel[i][j] == 'W':
					# self.ground_grid[i][j].water = 100
					# TODO: maybe the depth should vary
					self.grid[i][j] = WaterBody(100)
				if grid_excel[i][j] == 'O':
					self.ground_grid[i][j].is_obstacle = True
					self.grid[i][j] = Obstacle()

	def populate(self):
		"""
		Randomly spawn the agents into the grid. The chance is increased based
		on the population prior variable.
		"""
		SPAWN_CHANCE = self.agent_population/(self.env_size*self.env_size)
		for i in range(self.env_size):
			for j in range(self.env_size):
				if SPAWN_CHANCE > random.random() and self.grid[i][j] == None:
					new_agent = self.god_spawn((i,j))
					self.agents.add(new_agent)
					self.grid[i][j] = new_agent

	def god_spawn(self, position):
		"""
		Generate a new random Agent with AGENT_SKILL_POINTS total stats
		"""

		random_stats = np.random.randint(0,100,5)
		skills = np.ceil((random_stats/random_stats.sum())*AGENT_SKILL_POINTS)
		skills = remove_skills_overflow(skills, AGENT_SKILL_POINTS)

		strength, speed, sense, intelligence, sociability = tuple(skills)

		AgentType = random.choice([Herbivore, Omnivore, Predator])
		new_agent = AgentType(self.grid, self.agents, strength, speed, sense, intelligence, sociability)
		new_agent.position = position
		return new_agent

	def	agents_action(self):
		"""
		Call the agents to make actions.
		"""
		agents = list(self.agents)
		for agent in agents:
			agent.make_decision()

	def	update_agents(self):
		"""
		Every iteration increase the agents' hunger, thirst and sexual desire,
		decrease their energy and handle the death of agents
		"""
		killed = set()
		death_causes = {"energy":0, "hunger":0, "thirst":0}
		for agent in self.agents:
			agent.sexual+=1
			agent.energy-=1

			gcell = self.ground_grid[agent.position[0]][agent.position[1]]
			scell = self.sky_grid[agent.position[0]][agent.position[1]]
			# Preserves food resources while increasing the hunger
			if isinstance(agent, Predator):
				hunger = HUNGER_FACTOR_PREDATOR
			elif isinstance(agent, Omnivore):
				hunger = HUNGER_FACTOR_OMNIVORE
			else:
				hunger = HUNGER_FACTOR_HERBIVORE
			agent_nutri, gcell.nutrition = try_transfer(100 - agent.hunger, gcell.nutrition, hunger)
			agent.hunger = 100 - agent_nutri

			# Preserves water resources while increasing the thirst
			agent_water, scell.water = try_transfer(100 - agent.thirst, scell.water, 1)
			agent.thirst = 100 - agent_water

			is_alive, cause = agent.alive()
			if not is_alive:
				self.grid[agent.position[0]][agent.position[1]] = Meat(self.get_meat_food_bonus(agent))
				self.sky_grid[agent.position[0]][agent.position[1]].water += 100 - agent.thirst
				killed.add(agent)
				death_causes[cause] += 1

		self.agents.difference_update(killed)

		self.data_analyzer.add_death_causes(death_causes)
		self.data_analyzer.update()

	# Render the environment using the passed renderer
	def render(self, renderer):
		# Ground Grid:
		for y, cell_row in enumerate(self.ground_grid):
			for x, cell in enumerate(cell_row):
				t = cell.health / 100
				renderer.set_pixel(x, y, (lin_interp(55, 50, t), lin_interp(105, 100, t), 0))

		# Grid:
		for y, cell_row in enumerate(self.grid):
			for x, cell in enumerate(cell_row):
				if cell is not None:
					if isinstance(cell, Obstacle):
						renderer.set_pixel(x, y, (0, 0, 0))
					elif isinstance(cell, Plant):
						renderer.set_pixel(x, y, (0, 180, 0))
					elif isinstance(cell, WaterBody):
						renderer.set_pixel(x, y, (0, 0, 255))
					elif isinstance(cell, Meat):
						renderer.set_pixel(x, y, (155, 103, 60))

		# Agents:
		for agent in self.agents:
			if isinstance(agent, Herbivore):
				color = (250, 253, 15)
			elif isinstance(agent, Omnivore):
				color = (106, 13, 173)
			else:
				color = (255, 10, 10)

			renderer.set_pixel(agent.position[1], agent.position[0], color)

		# Sky Grid:
		for y, cell_row in enumerate(self.sky_grid):
			for x, cell in enumerate(cell_row):
				t = cell.water / 100
				color = renderer.get_pixel(x, y)
				added = lin_interp(0, 150, t)
				color = (color[0] + added, color[1] + added, color[2] + added)
				color = (clamp(color[0], 0, 255), clamp(color[1], 0, 255), clamp(color[2], 0, 255))

				renderer.set_pixel(x, y, color)

	def get_total_water(self):
		sum_water = 0
		for i in range(self.env_size):
			for j in range(self.env_size):
				sum_water += self.sky_grid[i][j].water + self.ground_grid[i][j].water
				cell = self.grid[i][j]
				if isinstance(cell, WaterBody):
					sum_water += cell.depth

		return sum_water

	# experimented with how to balance the food, it's interesting to see the
	# effect of this on the overall population
	def get_meat_food_bonus(self, agent):
		# base_bonus = agent.food_bonus
		# hunger_clamp = min(agent.hunger, 100)
		# return base_bonus * (1 - hunger_clamp/100)
		return agent.food_bonus
