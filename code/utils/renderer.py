import pygame

class Renderer:
    SCALE = 10

    def __init__(self, width, height):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((Renderer.SCALE * self.width, Renderer.SCALE * self.height))
        self.field = pygame.Surface((self.width,self.height))
        self.field.fill((255, 255, 255))
        # To scale the surface to a larger size
        self.dst_field = pygame.Surface((Renderer.SCALE * self.width, Renderer.SCALE * self.height))

    # color-format is: (r, g, b) where r,g,b in [0, 255]
    def set_pixel(self, x, y, color):
        self.field.set_at((x, y), color)

    # color-format is: (r, g, b) where r,g,b in [0, 255]
    def get_pixel(self, x, y):
        r, g, b, _ = self.field.get_at((x, y))
        return (r, g, b)

    def draw(self):
        self.screen.fill((0, 0, 0))
        pygame.transform.scale(self.field, (Renderer.SCALE *self.width, Renderer.SCALE * self.height), self.dst_field)
        self.screen.blit(self.dst_field, dest = (0,0))
        pygame.display.flip()

    def make_image(self, filename):
        pygame.image.save(self.dst_field, filename)
