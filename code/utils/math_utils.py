import numpy as np
import random
import math

from utils.constants import *

def sign(x):
    s = -1 if x < 0 else (1 if x > 0 else 0)
    return s

def clamp(val, min_val = 0, max_val = 100):
    return max(min(val, max_val), min_val)

# Transfers diff from src to diff while keeping the boundaries in takt
# returns new_src, new_dst
def try_transfer(src, dst, diff, max_val = 100, min_val = 0):
    new_dst = clamp(dst + diff)
    new_src = clamp(src - diff)
    diff = sign(diff) * min(abs(new_dst - dst), abs(src - new_src))
    return src - diff, dst + diff

# t in [0, 1]
def lin_interp(start, end, t):
	t = min(max(t, 0.0), 1.0)
	return start + (end - start) * t

def distance(pos1, pos2): # max norm
    return max(abs(pos1[0] - pos2[0]),abs(pos1[1] - pos2[1]))

def remove_skills_overflow(skills, max_skills):
    """
    If the total amount of points in the genes is not the targeted amount,
    fill or remove from it randomly to get to the right L1 length 
    """
    num_skills = len(skills)

    sum_skills = int(np.sum(skills))
    difference = sum_skills - max_skills # total overflow or underflow

    if difference == 0:
        return skills

    non_zero_skills = np.nonzero(skills)[0].tolist()
    for _ in range(abs(difference)+1):
        random_index = random.choice(non_zero_skills)
        skills[random_index] -= sign(difference)

    return skills
