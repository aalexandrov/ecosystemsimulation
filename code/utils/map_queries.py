import math

from environment_objects import Plant, WaterBody, Obstacle
from utils.math_utils import *

def get_closest_with(predicate, grid, position, vision, get_mate=-1, get_empty=False):
    """
    Search for the closest object of full filling the predicate in clockwise manner starting from
    the squares of distance 1 and continue until the end of the vision grid
    Depending on type you have to input normal grid or ground grid
    """
    directions = [(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1)]
    actor = grid[position[0]][position[1]]
    dist_min, closest_pos = -1, (-1, -1)
    for col in range(-vision, vision):
        for row in range(-vision, vision):
            new_y = position[0] + col
            new_x = position[1] + row
            check_square = None
            try:
                check_square = grid[new_y][new_x]
                if new_y < 0 or new_y > len(grid) or new_x < 0 or new_x > len(grid):
                    raise IndexError("Outside of grid")
            except IndexError:
                continue

            if get_empty:
                if check_square is not None:
                    if distance(position, (new_y, new_x)) < dist_min or dist_min == -1:
                        dist_min = distance(position, (new_y, new_x))
                        closest_pos = (new_y, new_x)

            elif get_mate != -1:
                if check_square is not None and predicate(check_square) and check_square != actor:
                    if check_square.sexual >= get_mate:
                        if distance(position, (new_y, new_x)) < dist_min or dist_min == -1:
                            dist_min = distance(position, (new_y, new_x))
                            closest_pos = (new_y, new_x)


            elif predicate(check_square) and distance(position, (new_y, new_x)) <= vision:
                if distance(position, (new_y, new_x)) < dist_min or dist_min == -1:
                    dist_min = distance(position, (new_y, new_x))
                    closest_pos = (new_y, new_x)

    if dist_min == -1:
        return None
    else:
        return closest_pos

def get_closest(t, grid, position, vision, get_mate=-1, get_empty=False):
    return get_closest_with((lambda cell, t=t: isinstance(cell, t)), grid, position, vision, get_mate, get_empty)

def get_direction(position1, position2):
    """
    Get the direciton of the object in position2 with respect to the object in position1
    Choose the direction based on the slope; if the slope is closer to the diagonal
    choose the diagonal, if it is closer to one of the axes, choose to go in a straigh line along it.
    """
    y = position2[0] - position1[0]
    x = position2[1] - position1[1]

    if x == 0 and y == 0:
        xslope = 0
        yslope = 0
    elif x == 0 and y!=0:
        xslope = 0
        yslope = 1
    elif y == 0 and x!=0:
        xslope = 1
        yslope = 0
    else:
        xslope = max(0,min(1,abs(round(x/y))))
        yslope = max(0,min(1,abs(round(y/x))))

    direction = (sign(y)*yslope, sign(x)*xslope)
    return direction

def neighbours(location1,location2):
    return abs(location1[0] - location2[0]) <= 1 and abs(location1[1] - location2[1]) <= 1
