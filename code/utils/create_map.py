import random
import xlsxwriter
import pandas as pd
import time

def map_to_excel(grid, filename):
    """ save map as excel file """
    workbook = xlsxwriter.Workbook(filename)
    worksheet = workbook.add_worksheet()
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            worksheet.write(i, j, grid[i][j])
    workbook.close()

def map_from_excel(filename):
    """ load map from excel file """
    grid = pd.read_excel(filename, header=None, index_col=None)
    return grid.values.tolist()

def get_empty_map(size):
    """ return 2d list with dimension 'size' and initialize with '_' representing an empty cell """
    return [['_' for i in range(size)] for j in range(size)]

def add_obstacles(grid, density):
    """ add random obstacles represented by 'O' to the map, with given density
    ie. density = 0.2 means 20% of the map will be obstacles """
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if random.random() < density:
                grid[i][j] = 'O'

def add_plants(grid, density):
    """ add random plants represented by 'P' to the map, with given density
    ie. density = 0.2 means 20% of the map will be plants """
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if random.random() < density:
                grid[i][j] = 'P'

def add_lake(grid, size, position):
    """ add a lake to the map, with given size and position """
    for i in range(size):
        for j in range(size):
            if position[0] + i < len(grid) and position[1] + j < len(grid):
                grid[position[0] + i][position[1] + j] = 'W'


# grid = get_empty_map(size=50)
# add_obstacles(grid, density=0.05)
# add_plants(grid, density=0.05)
# add_lake(grid, size=2, position=(5,5))
# add_lake(grid, size=3, position=(10,12))
# add_lake(grid, size=2, position=(6,20))
# add_lake(grid, size=4, position=(21,11))
# add_lake(grid, size=5, position=(25,25))
# add_lake(grid, size=5, position=(41, 5))
# add_lake(grid, size=3, position=(14,36))
# add_lake(grid, size=3, position=(44,42))
# add_lake(grid, size=4, position=(27,40))
# add_lake(grid, size=4, position=(42,24))
#
# map_to_excel(grid, 'utils/map50.xlsx')
# map2 = map_from_excel('utils/map50.xlsx')
#
# visualizer = Visualizer()
# visualizer.update(map2)
#
# time.sleep(10.0)
