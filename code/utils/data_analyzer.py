import matplotlib.pyplot as plt
import pandas as pd
from environment_objects import *
from agent import *

class Data():
    def __init__(self, agents, ground_grid, grid):
        self.agents = agents
        self.ground_grid = ground_grid
        self.grid = grid
        # tracked data
        self.population_size_l = []
        self.avg_thirst_l = []
        self.avg_hunger_l = []
        self.avg_energy_l = []
        self.avg_sexual_l = []

        self.avg_strength_l = []
        self.avg_speed_l = []
        self.avg_sense_l = []
        self.avg_intelligence_l = []
        self.avg_sociability_l = []

        self.avg_plant_amount_l = []
        self.water_amount_l = []
        self.death_energy_l = [0]
        self.death_hunger_l = [0]
        self.death_thirst_l = [0]

        self.herbivores = []
        self.omnivores = []
        self.predators = []

        # init graph
        plt.ion()
        fig = plt.figure()

        self.count = 0

    def update(self):
        self.population_size_l.append(len(self.agents))
        avg_thirst, avg_hunger, avg_energy, avg_sexual, avg_strength, avg_speed, avg_sense, avg_intelligence, avg_sociability = self.get_avgs()
        avg_plan_amount = self.get_plant_amount()
        avg_water_amount = self.get_water_amount()
        self.get_agent_types()
        self.avg_thirst_l.append(avg_thirst)
        self.avg_hunger_l.append(avg_hunger)
        self.avg_energy_l.append(avg_energy)
        self.avg_sexual_l.append(avg_sexual)
        self.avg_strength_l.append(avg_strength)
        self.avg_speed_l.append(avg_speed)
        self.avg_sense_l.append(avg_sense)
        self.avg_intelligence_l.append(avg_intelligence)
        self.avg_plant_amount_l.append(avg_plan_amount)
        self.water_amount_l.append(avg_water_amount)
        self.avg_sociability_l.append(avg_sociability)

        self.count += 1
        if self.count > 100:
            self.count = 0
            self.plot()
            self.to_csv()

    def get_plant_amount(self):
        count_plant_bonus = 0
        for row in range(len(self.ground_grid)):
            for col in range(len(self.ground_grid[row])):
                cell = self.grid[row][col]
                if isinstance(cell, Plant):
                    count_plant_bonus += self.grid[row][col].food_bonus
        return count_plant_bonus

    def get_water_amount(self):
        count_water_amount = 0
        for row in range(len(self.ground_grid)):
            for col in range(len(self.ground_grid[row])):
                cell = self.grid[row][col]
                if isinstance(cell, WaterBody):
                    count_water_amount += self.grid[row][col].depth
        return count_water_amount

    def add_death_causes(self, death_causes):
        self.death_energy_l.append(self.death_energy_l[-1]+death_causes["energy"])
        self.death_thirst_l.append(self.death_thirst_l[-1]+death_causes["thirst"])
        self.death_hunger_l.append(self.death_hunger_l[-1]+death_causes["hunger"])


    def get_avgs(self):
        avg_thirst = 0
        avg_hunger = 0
        avg_energy = 0
        avg_sexual = 0
        avg_strength = 0
        avg_speed = 0
        avg_sense = 0
        avg_sociability = 0
        avg_intelligence = 0

        for agent in self.agents:
            avg_thirst += agent.thirst / len(self.agents)
            avg_hunger += agent.hunger / len(self.agents)
            avg_energy += agent.energy / len(self.agents)
            avg_sexual += agent.sexual / len(self.agents)
            avg_strength += agent.strength / len(self.agents)
            avg_speed += agent.speed / len(self.agents)
            avg_sense += agent.sense / len(self.agents)
            avg_sociability += agent.sociability / len(self.agents)
            avg_intelligence += agent.intelligence / len(self.agents)

        return avg_thirst, avg_hunger, avg_energy, avg_sexual, avg_strength, avg_speed, avg_sense, avg_intelligence, avg_sociability
    def get_agent_types(self):
        herbivores = 0
        omnivores = 0
        predators = 0
        for agent in self.agents:
            if isinstance(agent,Herbivore):
                herbivores +=1
            elif isinstance(agent,Omnivore):
                omnivores +=1
            elif isinstance(agent,Predator):
                predators +=1


        self.herbivores.append(herbivores)
        self.omnivores.append(omnivores)
        self.predators.append(predators)

    def to_csv(self):
        df = pd.DataFrame(columns=['population', 'thirst', 'hunger', 'energy','sexual', 'strength', 'speed', 'sense',
         'intelligence', 'sociability', 'Energy Death', 'Hunger Death', 'Thirst Death', 'Water Amount', 'Plant Amount','Herbivores', 'Omnivores', 'Predators'])

        df['population'] = self.population_size_l
        df['thirst'] = self.avg_thirst_l
        df['hunger'] = self.avg_hunger_l
        df['energy'] = self.avg_energy_l
        df['sexual'] = self.avg_sexual_l
        df['strength'] = self.avg_strength_l
        df['speed'] = self.avg_speed_l
        df['sense'] = self.avg_sense_l
        df['plant amount'] = self.avg_plant_amount_l
        df['Intelligence'] = self.avg_intelligence_l
        df['Death Energy'] = self.death_energy_l[1:]
        df['Death Hunger'] = self.death_hunger_l[1:]
        df['Death Thirst'] = self.death_thirst_l[1:]
        df['Water Amount'] = self.water_amount_l
        df['sociability'] = self.avg_sociability_l
        df['Herbivores'] = self.herbivores
        df['Omnivores'] = self.omnivores
        df['Predators'] = self.predators

        df.to_csv('data.csv', index=False)


    def plot(self):
        plt.clf()
        # set titles
        plots = [['population', 'thirst', 'hunger', 'energy'],
        ['sexual', 'strength', 'speed', 'sense'],
        ['intelligence', 'sociability', 'Energy Death', 'Hunger Death'],
        ['Thirst Death', 'Water Amount', 'Plant Amount', 'Herbivores'],
        ['Omnivores', 'Predators', '_', '_']]

        for i in range(5):
            for j in range(4):
                plt.subplot(5,4,i*4+j+1).set_title(plots[i][j])

        # plot data
        plt.subplot(5,4,1).plot(self.population_size_l)
        plt.subplot(5,4,2).plot(self.avg_thirst_l)
        plt.subplot(5,4,3).plot(self.avg_hunger_l)
        plt.subplot(5,4,4).plot(self.avg_energy_l)
        plt.subplot(5,4,5).plot(self.avg_sexual_l)
        plt.subplot(5,4,6).plot(self.avg_strength_l)
        plt.subplot(5,4,7).plot(self.avg_speed_l)
        plt.subplot(5,4,8).plot(self.avg_sense_l)
        plt.subplot(5,4,9).plot(self.avg_intelligence_l)
        plt.subplot(5,4,10).plot(self.avg_sociability_l)
        plt.subplot(5,4,11).plot(self.death_energy_l)
        plt.subplot(5,4,12).plot(self.death_hunger_l)
        plt.subplot(5,4,13).plot(self.death_thirst_l)
        plt.subplot(5,4,14).plot(self.water_amount_l)
        plt.subplot(5,4,15).plot(self.avg_plant_amount_l)
        plt.subplot(5,4,16).plot(self.herbivores)
        plt.subplot(5,4,17).plot(self.omnivores)
        plt.subplot(5,4,18).plot(self.predators)

        plt.subplots_adjust(
                    wspace=0.7,
                    hspace=1.2)

        plt.draw()
        plt.show()
