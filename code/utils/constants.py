AGENT_SKILL_POINTS = 250
BASE_DECISION_PROBABILITY = 0.7 # controlls how random their movements are based on intelligence
BASE_DAMAGE = 10 # how much damage agents take in a fight at least
MAX_VISION = 10 # the vision range at maximum sense
MAX_MOVEMENT_COOLDOWN = 5 # how long an agent has to wait before moving again if their speed is below 10; at max speed the cooldown is none
MAX_MEMORY = 5 # how many previously visited cells the agent remembers (based on intelligence)
MAX_MUTATION_SIZE = 5 # how much a skill can mutate in the offspring

THIRST_THRESHOLD = 40
HUNGER_THRESHOLD = 40
ENERGY_THRESHOLD = 50
MATING_THRESHOLD = 80

WATER_AMOUNT = 100000 # the total amount of water in the environment
WATER_SPREAD_SPEED = 0.05 # speed of water spreading to neighbour cells (initialy 0.05)
NUTRITION_SPREAD_SPEED = 0.001 # speed of nutrition spreading in the ground cells
HUNGER_FACTOR_PREDATOR = 0.65 # Hunger increase
HUNGER_FACTOR_OMNIVORE = 1.5
HUNGER_FACTOR_HERBIVORE = 1.3
PLANT_SPAWN_PROBABILITY = 0.0003 # probability to try and spawn new plant at a specific cell (initially 0.0002)
WATER_FOR_PLANT_THRESHOLD = 0 # minimum water level required to spawn a new plant (initially 0)
WATER_FOR_PLANT_SUBTRACTION = 0 # water subtracted from ground to make a new plant (initially 0)
WATER_SPAWN_THRESHOLD = 50 # how much water is needed for the ground to become water
PLANT_SPAWN_THRESHOLD = 70 # how healthy the gound needs to be to spawn a plant
CONDENSATION_RATE = 0.01 # factor of condensation from ground to sky
REGENERATION_RATE_HEALTH_FACTOR = 0.1 # for ground cells health
MEAT_DECAY = 5 # how fast meat will decay into the ground
