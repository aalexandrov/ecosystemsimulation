import argparse
import pygame
import sys
import time
import numpy as np
import random

from environment import *
from agent import *
from utils.renderer import *

random.seed(9)
np.random.seed(9)

save_steps = [200, 10]

parser = argparse.ArgumentParser()
parser.add_argument(
    "--debug",
    "-d",
    default=False,
    action='store_true'
)
parser.add_argument(
    "--map",
    "-m",
    default=50,
)
args = parser.parse_args()

def main():
    if args.map == 50:
        WIDTH = 50
        HEIGHT = 50
        POP = 200
        terrain = './utils/map50.xlsx'
        # terrain = 'ecosystemsimulation/utils/map50.xlsx'
    else:
        WIDTH = 100
        HEIGHT = 100
        POP = 400
        terrain = './utils/map.xlsx'
        # terrain = 'ecosystemsimulation/utils/map.xlsx'

    if args.debug:
        WIDTH = 10
        HEIGHT = 10
        POP = 4

    environment = Environment(WIDTH, POP, terrain, debug_mode=args.debug)
    environment.generate()
    environment.populate()

    renderer = Renderer(WIDTH, HEIGHT)

    for i in range(10005):
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                pygame.quit()
                sys.exit()

        environment.agents_action()
        environment.update_agents()
        environment.update()
        # if args.debug:
        #     for agent in environment.agents:
        #         print(agent)

        environment.render(renderer)
        renderer.draw()

        # if i == 200 or i%1000 == 0:
        #     renderer.make_image(f'images/{i}.png')

        time.sleep(0.01)

    time.sleep(1000)

if __name__ == '__main__':
    main()
