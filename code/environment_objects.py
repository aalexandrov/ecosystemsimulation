from utils.math_utils import *
from utils.constants import *

class Plant:
	def __init__(self, food_bonus):
		self.food_bonus = food_bonus

class Meat:
	def __init__(self, food_bonus):
		self.food_bonus = food_bonus

class WaterBody:
	def __init__(self, depth):
		self.depth = depth

class Obstacle:
	def __init__(self):
		pass

class GroundCell:
	def __init__(self, water, nutrition):
		self.health = 100			# [0, 100]
		self.water = water			# [0, 100]
		self.nutrition = nutrition	# [0, 100]

	def update(self):
		regeneration_rate = min(self.nutrition, self.water) * REGENERATION_RATE_HEALTH_FACTOR
		self.nutrition, self.health = try_transfer(self.nutrition, self.health, regeneration_rate)

class SkyCell:
	def __init__(self, water):
		self.water = water 	# [0, 100]
